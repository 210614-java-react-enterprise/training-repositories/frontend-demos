/*
var x = 0;
var x = 20;
console.log(x);
*/
/*
let x = 0;
x = 20;
console.log(x);
*/
/*
const x = 0;
x = 20;
console.log(x);
*/

// type coercion
/*
console.log(7+"20");
console.log(7*"20");
console.log(7*"twenty");
*/

// truthy/falsy
// falsy: 0, "", null, undefined, NaN, false 
/*
console.log(new Boolean(5));
console.log(new Boolean("hello"));
console.log(new Boolean(0));
console.log(new Boolean(null));


let obj = {};
if(obj!=null){
    //do something
}

if(obj){
    //do something 
}

let arr = [3,2];
if(arr.length){
    // do something
}
*/

// object literal
let cat = {
    name: "Fluffy",
    colors: ["white", "grey"],
    furType: "long hair",
    isAdopted: false,
    age: 2
}

// class syntax for creating objects 
class Cat {
    constructor(name, colors, furType, isAdopted, age){
        this.name = name || "Not yet named";
        this.colors = colors || [];
        this.furType = furType || "Unknown"
        this.isAdopted = isAdopted || false;
        this.age = age;
    }

}

// constructor function 
function Cat1(name, colors, furType, isAdopted, age){
    this.name = name || "Not yet named";
    this.colors = colors || [];
    this.furType = furType || "Unknown"
    this.isAdopted = isAdopted || false;
    this.age = age;
    this.meow = function(){
        console.log(this.name+ " meows");
    }
    // this.__proto__ = new Animal();
}


// using a closure to protect access to the local values, javascript's take on encapsulation 
function Cat2(){
    let name;
    let isAdopted;
    let age;

    this.getName = function(){
        return name;
    }

    this.setName = function(_name){
        if(_name){
            name = _name;
        }
    }
} 

let cat2 = new Cat("Ronda", ["black"], "short hair", true, 1);

function greet(cat1, cat2){
    if(cat1 && cat2){
        console.log(`${cat1.name} greeted ${cat2.name}`)
    } else {
        console.log("invalid cat provided");
    }
}


// using built in JSON parsing
    // stringify = object -> json
    // parse = json -> object 
let jsonCat = JSON.stringify(cat2);
console.log(jsonCat);

console.log(JSON.parse(jsonCat));


// declaring functions
function add1(x, y){
    return x+y;
}

let add2 = function(x,y){
    return x+y;
}

let add3 = (x,y) => x+y;

//template literals
let username = "user35";
let welcomeMessage = `Welcome ${username}
to our great app`;

